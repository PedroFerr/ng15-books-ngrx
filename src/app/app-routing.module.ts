import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { menuItemsVertDir } from './app-interfaces';

import { HomeComponent } from './pages/home/home.component';

const routes_init: Routes = [
    { path: '', redirectTo: '/home', pathMatch: 'full' },
    { path: 'home', component: HomeComponent, title: 'Welcome!' },
];

const routes_others: Routes = [];
for (const item of menuItemsVertDir) {
    routes_others.push(
        { path: item.routerLink.slice(1), component: item.component, title: item.title, data: item.bodyClass }
    );
}

const routes_end: Routes = [
    { path: 'contact', loadChildren: () => import('./lazy-loading/contact-mod/contact.module').then(m => m.ContactModule) },
    { path: '**', loadChildren: () => import('./lazy-loading/not-found-mod/not-found.module').then(m => m.NotFoundModule) }
];

@NgModule({
    imports: [RouterModule.forRoot(routes_init.concat(routes_others).concat(routes_end))],
    exports: [RouterModule]
})
export class AppRoutingModule { }
