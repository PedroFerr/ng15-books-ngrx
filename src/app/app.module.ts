import { ErrorHandler, NgModule, forwardRef } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';

import { AppRoutingModule } from './app-routing.module';
import { AppMaterialModule } from './app-material.module';
import { bookReducer } from './store/books.reducer';
import { BooksEffects } from './store/books.effects';

import { environment } from 'src/environments/environment';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CustomErrorHandler } from './custom-error-handler.service';
import { AppInterceptor } from './app.interceptor';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { HomeComponent } from './pages/home/home.component';

import { CartModule } from './features-modules/shopping-cart/cart.module';


@NgModule({
    bootstrap: [AppComponent],
    declarations: [
        AppComponent,
        ToolbarComponent,
        HomeComponent,
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        FormsModule, ReactiveFormsModule,
        AppMaterialModule,
        AppRoutingModule,

        StoreModule.forRoot({ books: bookReducer }),
        EffectsModule.forRoot([BooksEffects]),
        StoreDevtoolsModule.instrument({
            maxAge: 25,                         // Retains last 25 states
            logOnly: environment.production,    // Restrict extension to log-only mode
        }),

        CartModule,

    ],
    providers: [
        { provide: ErrorHandler, useClass: CustomErrorHandler },
        { provide: HTTP_INTERCEPTORS, useClass: AppInterceptor, multi: true },
    ],
})
export class AppModule { }
