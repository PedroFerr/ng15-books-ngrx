import { HttpClient } from '@angular/common/http';
import { Injectable } from "@angular/core"
import { FormGroup } from '@angular/forms';
import { catchError, Observable, throwError } from 'rxjs';

import { environment } from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})

export class EmailService {

    constructor(private http: HttpClient) {}

    postMsg(input: FormGroup): Observable<any> {
        return this.http
            .post<any>(environment.urlServerMail, input)
            // trace errors:
            .pipe(
                catchError(err => {
                    console.warn('Error handled by the Service!');
                    return throwError(() => err);    // () => new Error(`Couldn't load data...`))
                    // return throwError(() => {
                    //     console.log(`Error re-thrown by the Service!`);
                    //     return new Error(`Couldn't load data...`)
                    // });
                })
            );

    }
}
