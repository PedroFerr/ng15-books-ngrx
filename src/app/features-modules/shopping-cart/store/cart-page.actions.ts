import { createActionGroup, props } from '@ngrx/store';
import { CartItem } from '../../../app-interfaces';

export const CartPageActions = createActionGroup({
    source: 'Cart/Page',
    events: {
        'Decrease number of item in cart': props<{ cartItem: CartItem }>(),
        'Increase number of item in cart': props<{ cartItem: CartItem }>(),
        'Remove item from cart': props<{ cartItem: CartItem }>()
    }
})
