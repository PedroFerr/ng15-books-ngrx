import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppMaterialModule } from '../../app-material.module';

import { CartComponent } from './cart/cart.component';
import { CartItemComponent } from './cart/cart-item/cart-item.component';
import { NgPipesModule } from 'ngx-pipes';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { cartReducer } from './store/cart.reducer';
import { CartEffect } from './store/cart.effect';



@NgModule({
    declarations: [
        CartComponent,
        CartItemComponent
    ],
    exports: [
        CartComponent
    ],
    imports: [
        CommonModule,
        AppMaterialModule,
        EffectsModule.forFeature([CartEffect]),
        StoreModule.forFeature('cartFeature', cartReducer),
        NgPipesModule
    ]
})
export class CartModule {}
