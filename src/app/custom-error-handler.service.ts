import { HttpErrorResponse } from "@angular/common/http";
import { ErrorHandler, Injectable, NgZone } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";

@Injectable()
export class CustomErrorHandler implements ErrorHandler {

    constructor(private snackbar: MatSnackBar, private zone: NgZone) {}

    handleError(error:HttpErrorResponse) {
        this.zone.run(() => {
            this.snackbar.open(
                '😒 ERROR: ' + error.message,
                'OK',
                {
                    duration: undefined, // 5000
                    panelClass: ['black-snackbar']
                }
            );
            console.error('MSG ERROR:', error);
        })
    }
}
