import { Component } from '@angular/core';
import { MenuItem, MenuItemResponsive, menuItemsVertDir } from '../app-interfaces';

@Component({
    selector: 'app-toolbar',
    templateUrl: './toolbar.component.html',
    styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent {
    menuItems: any; // (MenuItem | MenuItemResponse)[]
    constructor() {
        this.menuItems = menuItemsVertDir;
    }

}
