import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { filter, Observable, of, switchMap } from 'rxjs';

import { Book } from 'src/app/app-interfaces';
import { selectAllBooks } from '../../store/books.selectors';
import { selectNumberOfCartItems } from '../../store/core.selector';

import * as BookActions from '../../store/books.actions';
import { GoogleBooksService } from 'src/app/services/books-service';
import { FormControl, FormGroup } from '@angular/forms';
import { CatalogPageActions } from '../../store/books.actions';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {

    imageLoader = true;

    searchForm = new FormGroup({
        bookName: new FormControl(),
    });
    isLoading = false;

    books$: Observable<Book[]> = this.store.pipe(select(selectAllBooks));
    books: Book[] = [];
    nBooks = 0;

    bookName = ''; // Search
    filteredOptions$: Observable<string[]> | undefined;
    selectedBooks: string[] = [];
    found: string[] | undefined;
    hint: string | undefined;

    currentBook: Book | any;
    currentIndex = -1;

    panelOpenState = false;

    constructor(private store: Store, private booksService: GoogleBooksService) {
        this.initConditions();
    }

    ngOnInit(): void {
        this.store.dispatch(BookActions.getBooks());
        // this.books$.subscribe(x=>console.log(x));
        this.initFilterSearch();
    }

    setCurrentBook(books: Book[], index: number): void {
        this.currentBook = books[index];
        this.currentIndex = index;

        // And go to "the book" on Google Books - with/without price
        window.open(this.currentBook.volumeInfo.infoLink, '_blank');
    }

    buyBook(book: Book) {
        // window.alert(`Congratulations!!\n\nYou've had bought a new "${book.id}" book!`);
        this.store.dispatch(CatalogPageActions.addItemToCart({ book }));
    }

    clearSearchField() {
        this.bookName = '';
        this.hint = '';
    }

    private initConditions() {
        this.books$.subscribe((books: Book[]) => {
            for (let i = 0; i < books.length; i++) {
                const ele = books[i];
                this.selectedBooks[i] = ele.volumeInfo.title;
            }

            // Take off dups:
            this.selectedBooks = this.selectedBooks.filter(
                (item: any, index: number) => {
                    if (this.selectedBooks.indexOf(item) == index) {
                        return item;
                    }
                }
            );
            console.log('Search array: ', this.selectedBooks);
        });
    }

    private initFilterSearch() {
        this.isLoading = true;
        this.filteredOptions$ = this.searchForm.get('bookName')?.valueChanges.pipe(
            filter((data: string) => data.trim().length > 0),
            switchMap((x: string) => {
                console.log('... and found: ', x);
                return x ? this.searchingValue(x.replace(/[\s]/g, '')) : of([]);
            })
        );
    }

    private searchingValue(value: string) {
        this.isLoading = false;
        return of(
            this.selectedBooks.filter(
                (x) => x.replace(/[\s]/g, '').toLowerCase().indexOf(value.toLowerCase()) === 0
            )
        );
    }
}
