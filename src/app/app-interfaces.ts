import { Type } from '@angular/core';
import { BlogComponent } from './pages/others/blog/blog.component';
import { DocsComponent } from './pages/others/docs/docs.component';
import { PricingComponent } from './pages/others/pricing/pricing.component';
import { SignupComponent } from './pages/others/signup/signup.component';
import { SlideshowComponent } from './pages/others/slideshow/slideshow.component';

export interface MenuItem {
    idx: number;
    label: string;
    icon: string;
    component: Type<any>;
    title: string;
    routerLink: string;
    bodyClass: { bodyClass: string }
}
export interface MenuItemResponsive {
    mob: boolean;
    tab: boolean;
    desk: boolean;
    largD: boolean;
}

export interface Book {
    id: string;
    title: string;
    volumeInfo: {
        title: string;
        authors: Array<string>;
        imageLinks: { thumbnail: string; }
        infoLink: string;
        canonicalVolumeLink: string;
        description: string;
        categories: Array<string>;
        industryIdentifiers: Array<{ type: string; identifier: string; }>
        publishedDate: string;
        publisher: string;
        ratingsCount: number;
    }
    accessInfo: {
        //     pdf: {
        //         isAvailable: boolean;
        //         acsTokenLink: string;
        //     }
        webReaderLink: string;
        //     epub: {
        //         isAvailable: boolean;
        //         acsTokenLink: string;
        //     }
    }
    saleInfo: {
        country: string;
        isEbook: boolean;
        buyLink?: string;
        listPrice?: { amount: number; currencyCode: string; }
        saleability?: string;
    }
    searchInfo: { textSnippet: string; }
}

export interface CartFeatureState {
    cartItems: CartItem[],
    numberOfItems: number
}
export interface CartItem {
    id: string;
    item: Book;
    qty: number;
}

// export interface Task {
//     id: number;
//     title: string;
// }

// \\ // \\ // \\ // \\ // \\ // \\ // \\ // \\ // \\ // \\ // \\ // \\ // \\ // \\
//
// IMPORTANT! 'const' MUST be declared on Interfaces!
// Error if declared on ANY component.
//
// --------------------------------------------------------------------------------
const menuItemsResponsive: MenuItemResponsive[] = [
    { mob: true, tab: true, desk: true, largD: true },
    { mob: false, tab: true, desk: true, largD: true },
    { mob: false, tab: false, desk: true, largD: true },
    { mob: false, tab: false, desk: false, largD: true },
    { mob: false, tab: false, desk: false, largD: false }
];

export const menuItemsVertDir: MenuItem[] = [
    {
        idx: 0,
        label: 'Sign Up',
        icon: 'login',
        component: SignupComponent,
        title: 'Sign up!',
        routerLink: '/sign-up',
        bodyClass: { bodyClass: 'sign-up' }
    },
    {
        idx: 3,
        label: 'Docs',
        icon: 'notes',
        component: DocsComponent,
        title: 'Docs',
        routerLink: '/docs',
        bodyClass: { bodyClass: 'docs' }
    },
    {
        idx: 2,
        label: 'Showcase',
        icon: 'slideshow',
        component: SlideshowComponent,
        title: 'Show case',
        routerLink: '/slideshow',
        bodyClass: { bodyClass: 'slideshow' }
    },
    {
        idx: 1,
        label: 'Pricing',
        icon: 'attach_money',
        component: PricingComponent,
        title: 'Pricing',
        routerLink: '/pricing',
        bodyClass: { bodyClass: 'pricing' }
    },
    {
        idx: 4,
        label: 'Blog',
        icon: 'rss_feed',
        component: BlogComponent,
        title: 'Blog',
        routerLink: '/blog',
        bodyClass: { bodyClass: 'blog' }
    }
];
menuItemsVertDir.sort((a, b) => a.idx - b.idx);

menuItemsVertDir.forEach((elem: any, idx: any) => {
    elem.mob = menuItemsResponsive[idx].mob;
    elem.tab = menuItemsResponsive[idx].tab;
    elem.desk = menuItemsResponsive[idx].desk;
    elem.largD = menuItemsResponsive[idx].largD;

});
