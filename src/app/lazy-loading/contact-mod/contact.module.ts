import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContactRoutingModule } from './contact-routing.module';
import { ContactComponent } from './contact/contact.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppMaterialModule } from 'src/app/app-material.module';

@NgModule({
	imports: [
		CommonModule,
		FormsModule, ReactiveFormsModule,

		AppMaterialModule,

		ContactRoutingModule,
	],
	declarations: [
        ContactComponent
    ],
	providers: [],
})
export class ContactModule { }
