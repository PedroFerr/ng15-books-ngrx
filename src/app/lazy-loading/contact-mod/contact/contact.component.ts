import { DOCUMENT } from '@angular/common';
import { Component, Inject, Renderer2, OnInit, OnDestroy } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { EmailService } from 'src/app/services/email-service';

@Component({
    selector: 'app-contact',
    templateUrl: './contact.component.html',
    styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit, OnDestroy {
    title='Do your Contacts!'

    classSubscription: Subscription;

    formData: FormGroup = new FormGroup({});
    formSuccess = false;
    loading = true;
    errors = null;
    err = null;

    constructor(
        private titleService: Title,
        private router: Router,
        private renderer: Renderer2,
        @Inject(DOCUMENT) private document: any,
        private fb: FormBuilder, private emailService: EmailService
    ) {
        this.titleService.setTitle(this.title);

        this.classSubscription = this.router.events.subscribe((event) => {
            this.renderer.setAttribute(this.document?.body, 'class', 'contact');
        });

    }

    ngOnInit(): void {
        this.formData = this.fb.group({
            name: new FormControl('', [Validators.required, Validators.minLength(4)]),
            email: new FormControl('', [Validators.required, Validators.email]),
            message: new FormControl('', [Validators.required, Validators.minLength(10)]),
        });
    }

    ngOnDestroy(): void {
        if (this.classSubscription) {
            this.classSubscription.unsubscribe();
        }
    }

    onSubmit() {
        console.log(this.formData.value);

        this.emailService.postMsg(this.formData.value)
            .subscribe(
                // data => console.error(data), err => this.errors = err.message
                data => this.loading = false
            );
        this.formData.reset();
        this.formSuccess = true;

    }
}
