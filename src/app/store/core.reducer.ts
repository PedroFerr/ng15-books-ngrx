import { createReducer, on } from '@ngrx/store';
import { ShellState } from './core.selector';
import { CatalogPageActions } from './books.actions';

export const initialState: ShellState = {
    cartOpen: false
};

export const shellReducer = createReducer(
    initialState,
    on(CatalogPageActions.openCart, () => ({
        cartOpen: true
    }))
);
