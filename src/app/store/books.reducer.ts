import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { Book } from 'src/app/app-interfaces';
import * as BookActions from './books.actions';

export interface BookState extends EntityState<Book> {
    error: string;

}

export const adapter: EntityAdapter<Book> = createEntityAdapter<Book>({ selectId: (book) => book.id });

export const initialState: BookState = adapter.getInitialState({
    error: ''
});

export const bookReducer = createReducer(
    initialState,
    on(BookActions.retrievedBooks, (state, { books }) => {
        return adapter.addMany(books, state)
    }),

    /**
     *
    on(BookActions.removeBooks, (state, { id }) => {
        return { ...state, employees: state.employees.filter(e => e.id !== id) };
    })
     */
);

export const { selectAll } = adapter.getSelectors();
