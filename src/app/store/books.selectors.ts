import { createFeatureSelector, createSelector } from "@ngrx/store";
import * as fromBooks from './books.reducer'

export const selectBooksState = createFeatureSelector<fromBooks.BookState>('books');

export const selectAllBooks = createSelector(
    selectBooksState,
    fromBooks.selectAll
);



// export const selectItemsState = (state: any) => state.itemsFeature;

// export const selectItems = createSelector(
//     selectItemsState,
//     (state: ItemsFeatureState | undefined) => {
//         return state?.items;
//     }
// );
