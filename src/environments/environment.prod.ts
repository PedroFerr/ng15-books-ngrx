export const environment = {
  production: true,
  urlServerMail: 'https://server-mail-miles-net.onrender.com/sendmail',
  urlGoogleBooks: 'https://www.googleapis.com/books/v1/volumes?maxResults=30&orderBy=relevance&q=oliver%20sacks'

};
